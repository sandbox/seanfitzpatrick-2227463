(function ($) {
  Drupal.behaviors.AdHocAlerts = {
    attach: function (context, settings) {
      var adHocCloseBtn = $('#ad-hoc-alert .adhoc-alert-close');

      var cookieHash = settings.ad_hoc_alerts.hash;

      // Check if we're even using a cookie value.
      if ($.cookie) {
          $('#block-ad-hoc-alerts-ad-hoc-alert-block').addClass('adhoc-alert-closed');
          // If cookie is not present, set it.
          var cookie = $.cookie('ad_hoc_alerts_closure');
          if (cookie != cookieHash) {
            $('#block-ad-hoc-alerts-ad-hoc-alert-block').removeClass('adhoc-alert-closed').addClass('adhoc-alert-open');
            $(adHocCloseBtn).click( function(e) {
              $(this).closest('#block-ad-hoc-alerts-ad-hoc-alert-block').removeClass('adhoc-alert-open').addClass('adhoc-alert-closed');
              // set the cookie
              $.cookie('ad_hoc_alerts_closure', cookieHash, { expires: 365, path: '/' });
            });
          }
      }

    },
  };
}(jQuery));
